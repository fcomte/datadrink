<!-- .slide: class="slide" -->

<!-- .slide: class="slide" -->
# Objectifs de cette présentation

1. Pourquoi une telle plateforme ?
2. Faire un tour d'horizon des concepts techniques de l'écosystème du SI datalab
3. Démo

---

<!-- .slide: class="slide" -->
## Pourquoi une telle plateforme ?

Infrastructure souvent un frein à l'innovation

1. un cloud pour la datascience qui vise l'autonomie 
2. une plateforme ouverte sur Internet et opensource 
3. pour des usages peu sensibles

---

<!-- .slide: class="slide" -->
## un cloud pour la datascience qui vise l'autonomie

Une offre cloud :
1. des ressources centralisées (300 cpus , 3 To RAM , 6 GPUs T4) => mi juin (600 Cpus, 6 Tos RAM, 18 GPUs T4)
2. du SAAS (software as a service) pour des services de datascience traitement et stockage ( Jupyter , RStudio , Spark, ELK, Postgres, Tensorflow ...) avec un accès navigateur.
3. et un accès aux couches basses de l'infrastructure pour plus d'autonomie

----

<!-- .slide: class="slide" -->
##  une plateforme ouverte sur Internet et opensource

1. inscription ouverte aux adresses en gouv.fr [https://datalab.sspcloud.fr](https://datalab.sspcloud.fr)
2. opensource : intégration de logiciel opensource et UX développement Insee [Onyxia](https://github.com/InseeFrLab/onyxia) , [SAAS](https://github.com/InseeFrLab/https://github.com/InseeFrLab/helm-charts-datascience) ...

=> vous pouvez donc contribuer à l'élaboration du catalogue de service ou réinstaller cette plateforme sur vos infrastructures.

----

<!-- .slide: class="slide" -->
##  pour des usages peu sensibles

1. formation aux outils de datascience ou aux outils de développement
- catalogue de formation : ( funcampR, Parcours R du MTES, tutoriaux spark... et vos futurs contributions )
- utilisé par les écoles ENSAE et ENSAI
2. projet ou experimentation sur données ouvertes ou peu sensible.

240 utilisateurs qui se sont loggués au moins une fois la semaine dernière. 

---

<!-- .slide: class="slide" -->
## tour d'horizon des concepts techniques

1. pilier traitement (processus) : containerisation , l'incontournable des plateformes data modernes => orchestration de container : kubernetes
2. pilier données : stockage S3 pour l'universalité de l'accès à la donnée et de sa compatibilité pour le calcul distribué (compatible hadoop) 
3. pilier UX : une IHM développé par l'Insee [https://datalab.sspcloud.fr](https://datalab.sspcloud.fr)
